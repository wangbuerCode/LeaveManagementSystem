<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>班级信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder">班级信息列表</strong>
			</div>
			<div class="padding border-bottom">
				<ul class="search" style="padding-left:10px;">
					<li><a class="button border-main icon-plus-square-o"
						href="page/instructor/classes_add.jsp">添加班级信息</a></li>
				</ul>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th>班号</th>
					<th>班名</th>
					<th>二级学院编号</th>
					<th>专业</th>
					<th>年级</th>
					<th>操作</th>
				</tr>
				<%--读取所有管理员信息记录 --%>
				<%
					//设置编码方式
					request.setCharacterEncoding("utf-8");
					//2.1 获得链接
					Connection conn = getConn();
					String sql = "select * from sys_classes";
					//2.3:由给定Connection类型的连接对象conn执行SQL指令的PrepareStatement类型的对象pstmt
					Statement stmt = conn.createStatement();
					//2.4:PrepareStatement类型的对象pstmt执行SQL查询语句并返回结果
					ResultSet rs = stmt.executeQuery(sql);
					//定义int序号变量
					while (rs.next()) {
						String classId = rs.getString("classId");
						String className = rs.getString("className");
						String depId = rs.getString("depId");
						String major = rs.getString("major");
						String grade = rs.getString("grade");
				%>

				<tr>
					<td><%=classId%></td>
					<td><%=className%></td>
					<td><%=depId%></td>
					<td><%=major%></td>
					<td><%=grade%></td>
					<td>
						<div class="button-group">
							 <a class="button border-red" href="<%=path%>/page/instructor/action/action_delClasses.jsp?classId=<%=classId%>"><span class="icon-trash-o"></span> 删除</a>
						</div>
					</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>

</body>
<script type="text/javascript">
$("[name='keywords']").each(function(){
var n = $(this).attr("search");
if(n.indexOf(name) == -1 )
{
$(this).hide();//隐藏不存在关键字的列表
}
});
</script>
</html>
