<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--删除用户--%>
<%
    //取出请求参数值之前设置请求的编码方式
      request.setCharacterEncoding("utf-8");
 	//接收参数
 	  int Count = 0;
      String instId = request.getParameter("instId");
      String instName = request.getParameter("instName");
      String depId = request.getParameter("depId");
      String telephone = request.getParameter("telephone");
     //获得数据库连接
      Connection coon = getConn();
      //创建语句
      String sql = "delete from sys_instructor where instId = ?";
      PreparedStatement ptmt = coon.prepareStatement(sql);
      ptmt.setString(1, instId);
      //执行更新
      Count = ptmt.executeUpdate();
  	//跳转到用户列表页面(instructor_list.jsp)
	process(request, response, "/page/instructor/instructor_list.jsp");
     close(null, ptmt, coon);
%>