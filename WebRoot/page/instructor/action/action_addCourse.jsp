<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
			 <% 
			 	//设置编码
			 	request.setCharacterEncoding("utf-8");
				//获取参数
				
				// 获取课程编号的随机数，调用一个方法
				String courseId = courseNum();
				String classId = request.getParameter("classId");
				String courseName = request.getParameter("courseName");
				String year = request.getParameter("year");
				String term = request.getParameter("term");
				String hour = request.getParameter("hour");
				int count = 0;
				
				//获得数据库连接 
				Connection coon = getConn();
				//创建语句 
				String sql = "insert into sys_course(courseId,classId,courseName,year,term,hour)"+
				"values(?,?,?,?,?,?)";
				PreparedStatement ptst = coon.prepareStatement(sql);
				ptst.setString(1, courseId);
				ptst.setString(2, classId);
				ptst.setString(3, courseName);
				ptst.setString(4, year);
				ptst.setString(5, term);
				ptst.setString(6, hour);
				//执行语句 
				count = ptst.executeUpdate();
				//3:跳转到用户列表页面(stu_list.jsp) 
				process(request, response,"/page/instructor/course_list.jsp"); 
				 close(null, ptst, coon);
				%>
