<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
    			 
    			String stuId = request.getParameter("stuId");
				String classId = request.getParameter("classId");
				String stuName = request.getParameter("stuName");
				String sex = request.getParameter("sex");
				String address = request.getParameter("address");
				String stuTel = request.getParameter("stuTel");
				String contact = request.getParameter("contact");
				String contactTel = request.getParameter("contactTel");
				String password = request.getParameter("password");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //创建语句
   	String sql = "update sys_student set classId=?,stuName=?,sex=?,address=?,stuTel=?,contact=?,contactTel=?,password=?"
			 	+ "where stuId = ?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    //赋值
    pstmt.setString(1, classId);
    pstmt.setString(2, stuName);
    pstmt.setString(3, sex);
    pstmt.setString(4, address);
    pstmt.setString(5, stuTel);
    pstmt.setString(6, contact);
    pstmt.setString(7, contactTel);
    pstmt.setString(8, password);
    pstmt.setString(9, stuId);
    //执行语句
    count = pstmt.executeUpdate();
  	//跳转到用户列表页面(stu_list.jsp)
	  process(request, response, "/page/instructor/stu_list.jsp");
	   close(null, pstmt, coon);
%>