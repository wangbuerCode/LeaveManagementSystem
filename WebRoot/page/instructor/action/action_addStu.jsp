<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
			 <% 
			 	//设置编码
			 	request.setCharacterEncoding("utf-8");
				//获取参数
				String stuId = request.getParameter("stuId");
				String classId = request.getParameter("classId");
				String stuName = request.getParameter("stuName");
				String sex = request.getParameter("sex");
				if(sex.equals("0")) {
					sex = "男";
				}else {
					sex = "女";
				}
				
				String address = request.getParameter("address");
				String stuTel = request.getParameter("stuTel");
				String contact = request.getParameter("contact");
				String contactTel = request.getParameter("contactTel");
				String password = request.getParameter("password");
				int count = 0;
				
				//获得数据库连接 
				Connection coon = getConn();
				//创建语句 
				String sql = "insert into sys_student(stuId,classId,stuName,sex,address,stuTel,contact,contactTel,password)"+
				"values(?,?,?,?,?,?,?,?,?)";
				PreparedStatement ptst = coon.prepareStatement(sql);
				ptst.setString(1, stuId);
				ptst.setString(2, classId);
				ptst.setString(3, stuName);
				ptst.setString(4, sex);
				ptst.setString(5, address);
				ptst.setString(6, stuTel);
				ptst.setString(7, contact);
				ptst.setString(8, contactTel);
				ptst.setString(9, password);
				//执行语句 
				count = ptst.executeUpdate();
				//3:跳转到用户列表页面(stu_list.jsp) 
				process(request, response,"/page/instructor/stu_list.jsp");
				 close(null, ptst, coon); 
				%>
