<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
			 <% 
			 	//设置编码
			 	request.setCharacterEncoding("utf-8");
				//获取参数
				String classId = request.getParameter("classId");
				String className = request.getParameter("className");
				String depId = request.getParameter("depId");
				String major = request.getParameter("major");
				String grade = request.getParameter("grade");
				int count = 0;
				
				//获得数据库连接 
				Connection coon = getConn();
				//创建语句 
				String sql = "insert into sys_classes(classId,className,depId,major,grade)"+
				"values(?,?,?,?,?)";
				PreparedStatement ptst = coon.prepareStatement(sql);
				ptst.setString(1, classId);
				ptst.setString(2, className);
				ptst.setString(3, depId);
				ptst.setString(4, major);
				ptst.setString(5, grade);
				//执行语句 
				count = ptst.executeUpdate();
				//3:跳转到用户列表页面(classes_list.jsp) 
				process(request, response,"/page/instructor/classes_list.jsp"); 
				 close(null, ptst, coon);
				%>
				