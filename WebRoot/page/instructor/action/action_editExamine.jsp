<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp" %>
<%@ include file="/base/database.jsp" %>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
 	  String leaveId = request.getParameter("leaveId");
	  String reason = request.getParameter("reason");
	  String daynum = request.getParameter("daynum");
	  String status = request.getParameter("status");
	  String audittime = request.getParameter("audittime");
	  //获取系统当前时间，付给教师审核时间
	  Date date = new Date();
	  Timestamp stamp = new Timestamp(date.getTime());
	  
	  String opinion = request.getParameter("opinion");
	  Connection conn = getConn();
	  int rs_daynum = 0;
	  String sql1 = "select * from sys_leave where leaveId = ?";
	  PreparedStatement pstmt1 = conn.prepareStatement(sql1);
	  pstmt1.setString(1, leaveId);
	  ResultSet rs1 = pstmt1.executeQuery();
	  if (rs1.next()) {
	  	rs_daynum = rs1.getInt("daynum");
	  }
	  
	  int state = 0;
	  if (status.equals("未审核")) {
	  	state = 0;
	  }else if(status.equals("同意")) {
	  	state = 1;
	  }else if(status.equals("不同意")) {
	  	state = 2;
	  }
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
 	//进行更新
   	String sql = "update sys_leave set reason=?,daynum=?,status=?,audittime=?,opinion=? where leaveId = ?";
    PreparedStatement pstmt = coon.prepareStatement(sql);
    pstmt.setString(1, reason);
    pstmt.setInt(2, rs_daynum);
    pstmt.setInt(3, state);
    pstmt.setTimestamp(4, stamp);
    pstmt.setString(5, opinion);
    pstmt.setString(6, leaveId);
    count = pstmt.executeUpdate();
  	//跳转到用户列表页面(examine_list.jsp)
	  process(request, response, "/page/instructor/examine_list.jsp");
	   close(null, pstmt, coon);
%>