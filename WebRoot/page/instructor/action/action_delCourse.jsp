<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--删除用户--%>
<%
	//取出请求参数值之前设置请求的编码方式
	request.setCharacterEncoding("utf-8");
	//接收参数
	int Count = 0;
	String courseId = request.getParameter("courseId");
	String classId = request.getParameter("classId");
	String courseName = request.getParameter("courseName");
	String year = request.getParameter("year");
	String term = request.getParameter("term");
	String hour = request.getParameter("hour");
	//获得数据库连接
	Connection coon = getConn();
	//创建语句
	String sql = "delete from sys_course where courseId = ?";
	PreparedStatement ptmt = coon.prepareStatement(sql);
	ptmt.setString(1, courseId);
	//执行更新
	Count = ptmt.executeUpdate();
	//跳转到用户列表页面(course_list.jsp)
	process(request, response, "/page/instructor/course_list.jsp");
	 close(null, ptmt, coon);
%>