<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
		String classId = request.getParameter("classId");
		String className = request.getParameter("className");
		String depId = request.getParameter("depId");
		String major = request.getParameter("major");
		String grade = request.getParameter("grade");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //创建语句
   	String sql = "update sys_classes set classId=?,className=?,depId=?,major=?,grade=?"
			 	+ "where classId = ?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    //赋值
    pstmt.setString(1, classId);
    pstmt.setString(2, className);
    pstmt.setString(3, depId);
    pstmt.setString(4, major);
    pstmt.setString(5, grade);
    pstmt.setString(6, classId);
    //执行语句
    count = pstmt.executeUpdate();
  	//跳转到用户列表页面(classes_list.jsp)
	  process(request, response, "/page/instructor/classes_list.jsp");
	   close(null, pstmt, coon);
%>