<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>请假信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> 请假列表</strong>
		</div>
		<div class="padding border-bottom">
				<ul class="search" style="padding-left:10px;">
					<form  name="searchmessage" action="page/instructor/export_search.jsp" method="post">
						<li><input type="text" placeholder="请选择班级名称" name="keywords" class="input" style="width:250px; line-height:17px;display:inline-block" /> 
						<li><input type="text" placeholder="请选择学期" name="keywordsone" class="input" style="width:250px; line-height:17px;display:inline-block" /> 
						<a href='javascript:document.searchmessage.submit();' class="button border-main icon-search" > 搜索</a></li>
					</form>
				</ul>
		</div>
		<table class="table table-hover text-center">
			<tr>
				<th>请假编号</th>
				<th>班级名称</th>
				<th>学期</th>
				<th>课程名称</th>
				<th>请假事由</th>
				<th>请假天数</th>
				<th>学号</th>
				<th>请假时间</th>
				<th>审核状态</th>
				<th>审核时间</th>
				<th>审核意见</th>
				<th>操作</th>
			</tr>

			<%--读取所有管理员信息记录 --%>
			<%
				//设置编码方式
				request.setCharacterEncoding("utf-8");
				String keywords = request.getParameter("keywords");
				String keywordsone = request.getParameter("keywordsone");
					System.out.println(keywords);
						System.out.println(keywordsone);
				//2.1 获得链接
				Connection conn = getConn();
				String sql = "";
				Statement stmt = null;
				ResultSet rs = null;
				if(keywords!=null  && keywordsone!=null) {
					sql = "select * from sys_classes a,sys_course b,sys_leave c "
					+ "where a.classId = b.classId and b.courseId = c.courseId and className = ?  and term = ?";
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, keywords);
					pstmt.setString(2, keywordsone);
					rs = pstmt.executeQuery();
				}else if(keywords==null | keywords=="" && keywordsone!=null) {
					sql = "select * from sys_classes a,sys_course b,sys_leave c "
					+ "where a.classId = b.classId and b.courseId = c.courseId and term = ?";
					PreparedStatement pstmt = conn.prepareStatement(sql);
				
					pstmt.setString(1, keywordsone);
					rs = pstmt.executeQuery();
				}else if(keywords!=null && keywordsone==null | keywords=="") {
					sql = "select * from sys_classes a,sys_course b,sys_leave c "
					+ "where a.classId = b.classId and b.courseId = c.courseId and className = ?";
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, keywords);
					rs = pstmt.executeQuery();
				}else if(keywords==null | keywords=="" && keywordsone==null | keywords=="") {
					sql = "select * from sys_classes a,sys_course b,sys_leave c "
					+ "where a.classId = b.classId and b.courseId = c.courseId ";
					stmt = conn.createStatement();
					rs = stmt.executeQuery(sql);
				}
				//定义一个HashMap
				List<HashMap<String, Object>> list = null;
				list = new ArrayList<HashMap<String, Object>>();
				HashMap<String, Object> map = null;
				while (rs.next()) {
					//实例化HashMap
					map = new HashMap<String, Object>();
					String leaveId = rs.getString("leaveId");
					String courseName = rs.getString("courseName");
					String reason = rs.getString("reason");
					int daynum = rs.getInt("daynum");
					String stuNo = rs.getString("stuNo");
					String qpplytime = rs.getString("qpplytime");
					int status = rs.getInt("status");
					String className = rs.getString("className");
					String term = rs.getString("term");
					String audittime = "";
					//这块是个坑，因为在数据库中audittime是date类型，而不是String类型，所以只能用
					//条件语句if(rs.getString("audittime")==null)，而不能用if(rs.getString("audittime").equal(null))
					if (rs.getString("audittime")==null){
						audittime = "审核中...";
					} else{
						audittime = rs.getString("audittime");
					}
					String opinion ="";
					if (status!=0 && rs.getString("opinion")==null){
						opinion = "无意见";
					} else if(status == 0 && rs.getString("opinion")==null){
						opinion = "暂无";
					}else{
						opinion = rs.getString("opinion");
					}
			%>
			<tr>
				<%
						String state = "";
						if (status == 0) {
							state = "待审核";
						} else if (status == 1) {
							state = "同意";
						} else if (status == 2) {
							state = "不同意";
						}
						if (state != null) {
				%>
				<%
					//将数据加到map中
							map.put("leaveId", leaveId);
							map.put("className", className);
							map.put("term", term);
							map.put("courseName", courseName);
							map.put("reason", reason);
							map.put("daynum", daynum);
							map.put("stuNo", stuNo);
							map.put("qpplytime", qpplytime);
							map.put("status", status);
							map.put("audittime", audittime);
							map.put("opinion", opinion);
							//将数据添加到list中
							list.add(map);
							String path1 = request.getRealPath("/") + "student.xls";
							export(list, path1);
				%>
				<td><%=leaveId%></td>
				<td><%=className%></td>
				<td><%=term%></td>
				<td><%=courseName%></td>
				<td><%=reason%></td>
				<td><%=daynum%></td>
				<td><%=stuNo%></td>
				<td><%=qpplytime%></td>
				<td><%=state%></td>
				<td><%=audittime%></td>
				<td><%=opinion%></td>
				<%} %>
				
				<td>
					<div class="button-group">
						<a class="button border-main"
							href="<%=path%>/page/instructor/examine_edit.jsp?leaveId=<%=leaveId%>"><span
							class="icon-edit"></span> 预览</a>
					</div>
				</td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div class="field" style="position: absolute; right: 10px;">
	<a class="button bg-main icon-check-square-o"
							href="/leave/student.xls"
							style="background-color:#FFB800; 
						type="submit">导出Excel文件</a>
	</div>

</body>
<script type="text/javascript">
	$("[name='keywords']").each(function() {
		var n = $(this).attr("search");
		if (n.indexOf(name) == -1) {
			$(this).hide(); //隐藏不存在关键字的列表
		}
	});
</script>
</html>
