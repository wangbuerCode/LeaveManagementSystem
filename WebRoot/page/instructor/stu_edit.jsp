<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<% 
   //接收参数
   String stuId = request.getParameter("stuId");
   
   //根据参数读取数据库响应记录
   Connection conn=getConn();
   String sql="select * from sys_student where stuId=?";
   PreparedStatement pstmt=conn.prepareStatement(sql);
   pstmt.setString(1, stuId);
   ResultSet rs=pstmt.executeQuery();
   String rs_stuId ="";
   String rs_classId  ="";
   String rs_stuName  ="";
   String rs_sex  ="";
   String rs_address ="";
   String rs_stuTel  ="";
   String rs_contact  ="";
   String rs_contactTel  ="";
   String rs_password ="";
   if(rs.next()){
	       rs_stuId = rs.getString("stuId");
	       rs_classId = rs.getString("classId");
	       rs_stuName = rs.getString("stuName");
	       rs_sex = rs.getString("sex");
	       rs_address = rs.getString("address");
	       rs_stuTel = rs.getString("stuTel");
	       rs_contact = rs.getString("contact");
	       rs_contactTel = rs.getString("contactTel");
	       rs_password = rs.getString("password");
	   
   }
   //使用表达式显示
   //跳转，无
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>用户信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head" id="add">
			<strong><span class="icon-pencil-square-o"></span>编辑用户</strong>
		</div>
		<div class="body-content">
			<form method="post" class="form-x"
				action="page/instructor/action/action_editStu.jsp">
				<div class="form-group">
					<div class="label">
						<label>学号：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" readonly="readonly"
							value="<%=rs_stuId %>" name="stuId"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>班级名称：</label>
					</div>
					<div class="field">
						<select name="classId" class="input w50">
							<%
								Connection coon = getConn();
								String sql1 = "select * from sys_classes a, sys_student b where a.classId = b.classId and stuId=?";
								pstmt=coon.prepareStatement(sql1);
  							    pstmt.setString(1, stuId);
								ResultSet rs1 = pstmt.executeQuery();
								while (rs1.next()) {
									String classId = rs1.getString("classId");
									String className = rs1.getString("className");
									String depId = rs1.getString("depId");
									String major = rs1.getString("major");
									String grade = rs1.getString("grade");
							%>
							<option value="<%=classId%>"><%=grade + major + className%></option>
								<%
								}
							%>
							
							<% 
								String sql2 = "select * from sys_classes  where classId != ?";
								pstmt=coon.prepareStatement(sql2);
  							    pstmt.setString(1, rs_classId);
								ResultSet rs2 = pstmt.executeQuery();
								while (rs2.next()) {
									String classId = rs2.getString("classId");
									String className = rs2.getString("className");
									String depId = rs2.getString("depId");
									String major = rs2.getString("major");
									String grade = rs2.getString("grade");
							%>
								<option value="<%=classId%>"><%=grade + major + className%>
								<%
								}
							%>
							</option>
						</select>

						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>姓名：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_stuName %>"
							name="stuName" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>用户类型：</label>
					</div>
					<div class="field" style="margin-top:10px;">
						<%if(rs_sex.equals("男")){ %>
						<input type="radio" name="sex" value="女" />女&emsp;
						<input type="radio"name="sex"  value="男" checked="checked" />男
						<%} else {%>
						<input type="radio" name="sex"checked="checked" value="女"/>女&emsp;
						<input type="radio" name="sex" value="男"/>男
						<%} %>

						<div class="tips"></div>
					</div>
				</div>

				<div class="form-group">
					<div class="label">
						<label>通讯地址：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_address %>"
							name="address" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>学生电话：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_stuTel %>"
							name="stuTel" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>联系人：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_contact %>"
							name="contact" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>

				<div class="form-group">
					<div class="label">
						<label>联系人电话：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_contactTel %>"
							name="contactTel" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>密码：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_password %>"
							name="password" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<button class="button bg-main icon-check-square-o" type="submit">
							提交</button>
						<a class="button bg-main icon-check-square-o"
							href="page/instructor/stu_list.jsp"
							style="background-color:#FFB800; 
						type="submit">返回</a>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>
