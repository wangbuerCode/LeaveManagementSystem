<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>添加课程信息</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head" id="add">
			<strong><span class="icon-pencil-square-o"></span>增加课程</strong>
		</div>
		<div class="body-content">
			<form method="post" class="form-x"
				action="page/instructor/action/action_addCourse.jsp">
				<div class="form-group">
					<div class="label">
						<label>班号：</label>
					</div>
					<div class="field">
						<select name="classId" class="input w50">
							<%
								Connection coon = getConn();
								String sql = "select * from sys_classes";
								Statement stmt = coon.createStatement();
								ResultSet rs = stmt.executeQuery(sql);
								while (rs.next()) {
									String classId = rs.getString("classId");
							%>
							<option value="<%=classId%>"><%=classId%>
							<%
								}
							%>
							</option>
						</select>

						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>课程名称：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="courseName"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>学年：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="year"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>学期：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="term"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>学时：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="hour"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<button class="button bg-main icon-check-square-o" type="submit">
							提交</button>
						&emsp;&emsp;&nbsp; <a class="button bg-main icon-check-square-o"
							href="page/instructor/course_list.jsp"
							style="background-color:#FFB800; 
						type="submit">返回</a>
						</from>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>
