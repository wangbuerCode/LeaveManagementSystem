<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>添加学生信息</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head" id="add">
			<strong><span class="icon-pencil-square-o"></span>增加用户</strong>
		</div>
		<div class="body-content">
			<form method="post" class="form-x"
				action="page/instructor/action/action_addStu.jsp">
				<div class="form-group">
					<div class="label">
						<label>学号：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="stuId"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>班级名称：</label>
					</div>
					<div class="field">
						<select name="classId" class="input w50">
							<%
								Connection coon = getConn();
								String sql = "select * from sys_classes";
								Statement stmt = coon.createStatement();
								ResultSet rs = stmt.executeQuery(sql);
								while (rs.next()) {
									String classId = rs.getString("classId");
									String className = rs.getString("className");
									String depId = rs.getString("depId");
									String major = rs.getString("major");
									String grade = rs.getString("grade");
							%>
							<option value="<%=classId%>"><%=grade + major + className%>
							<%
								}
							%>
							</option>
						</select>

						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>姓名：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="stuName"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>性别：</label>
					</div>
					<div class="field" style="margin-top:8px;">
						<label for="sex_f">女</label> <input type="radio" name="sex"
							id="sex_f" value="1" checked="checked" /> <label for="sex_m">男</label>
						<input type="radio" name="sex" id="sex_m" value="0" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>通讯地址：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="address"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>学生电话：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="stuTel"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>联系人：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="contact"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>联系人电话：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="contactTel"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>密码：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="password"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<button class="button bg-main icon-check-square-o" type="submit">
							提交</button>
						&emsp;&emsp;&nbsp; <a class="button bg-main icon-check-square-o"
							href="page/instructor/stu_list.jsp"
							style="background-color:#FFB800; 
						type="submit">返回</a>
						</from>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>
