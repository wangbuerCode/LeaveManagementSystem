<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
			 <% 
			 	//设置编码
			 	request.setCharacterEncoding("utf-8");
				//获取参数
				String userId = request.getParameter("userId");
				String fullname = request.getParameter("fullname");
				String password = request.getParameter("password");
				String telephone = request.getParameter("telephone");
				int count = 0;
				
				//获得数据库连接 
				Connection coon = getConn();
				//创建语句 
				String sql = "insert into sys_user(userId,fullname,password,telephone) values(?,?,?,?)";
				PreparedStatement ptst = coon.prepareStatement(sql);
				ptst.setString(1, userId);
				ptst.setString(2, fullname);
				ptst.setString(3, password);
				ptst.setString(4, telephone);
				//执行语句 
				count = ptst.executeUpdate();
				//3:跳转到用户列表页面(user_list.jsp) 
				process(request, response,"/page/user/user_list.jsp"); 
				close(null, ptst, coon);
				%>
