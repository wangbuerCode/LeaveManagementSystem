<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp" %>
<%@ include file="/base/database.jsp" %>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
      String userId = request.getParameter("userId");
      String password = request.getParameter("password");
      String fullname = request.getParameter("fullname");
      String telephone = request.getParameter("telephone");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //创建语句
   	String sql = "update sys_user set userId=?,password=?,fullname=?,telephone=?"
			 	+ "where userId = ?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    //赋值
    pstmt.setString(1, userId);
    pstmt.setString(2, password);
    pstmt.setString(3, fullname);
    pstmt.setString(4, telephone);
    pstmt.setString(5, userId);
    //执行语句
    count = pstmt.executeUpdate();
  	//跳转到登录页面
  	if(session.getAttribute("userid").equals(userId) && session.getAttribute("password").equals(password)==false) {
  	  response.sendRedirect("/leave/login.jsp");
  	} else {
  		process(request, response, "/page/user/user_list.jsp");
  	}
   close(null, pstmt, coon);
%>