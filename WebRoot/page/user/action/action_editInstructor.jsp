<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp" %>
<%@ include file="/base/database.jsp" %>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
      String instId = request.getParameter("instId");
      String instName = request.getParameter("instName");
      String depId = request.getParameter("depId");
      String telephone = request.getParameter("telephone");
      String email = request.getParameter("email");
      String password = request.getParameter("password");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //创建语句
   	String sql = "update sys_instructor set instId=?,instName=?,depId=?,telephone=?,email=?,password=?"
			 	+ "where instId = ?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    //赋值
    pstmt.setString(1, instId);
    pstmt.setString(2, instName);
    pstmt.setString(3, depId);
    pstmt.setString(4, telephone);
     pstmt.setString(5, email);
    pstmt.setString(6, password);
    pstmt.setString(7, instId);
    //执行语句
    count = pstmt.executeUpdate();
  	//跳转到登录页面
  	 if(session.getAttribute("userid").equals(instId)) {
  	  response.sendRedirect("/leave/login.jsp");
  	} else {
  		 process(request, response, "/page/user/instructor_list.jsp");
  	}
  	  close(null, pstmt, coon);
%>