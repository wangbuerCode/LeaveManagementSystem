<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp" %>
<%@ include file="/base/database.jsp" %>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
 	  String depId = request.getParameter("depId");
      String depName = request.getParameter("depName");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //创建语句
   	String sql = "update sys_department set depId=?,depName=? where depId=?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    //赋值
    pstmt.setString(1, depId);
    pstmt.setString(2, depName);
    pstmt.setString(3, depId);
    //执行语句
    count = pstmt.executeUpdate();
    
  	//跳转到登录页面
    process(request, response, "/page/user/dep_list.jsp");
     close(null, pstmt, coon);
%>