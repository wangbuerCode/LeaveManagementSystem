<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
 		String courseId = request.getParameter("courseId");
		String classId = request.getParameter("classId");
		String courseName = request.getParameter("courseName");
		String year = request.getParameter("year");
		String term = request.getParameter("term");
		String hour = request.getParameter("hour");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //创建语句
   	String sql = "update sys_course set classId=?,courseName=?,year=?,term=?,hour=?"
			 	+ "where courseId = ?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    //赋值
    pstmt.setString(1, classId);
    pstmt.setString(2, courseName);
    pstmt.setString(3, year);
    pstmt.setString(4, term);
    pstmt.setString(5, hour);
    pstmt.setString(6, courseId);
    //执行语句
    count = pstmt.executeUpdate();
  	//跳转到用户列表页面(course_list.jsp)
	  process(request, response, "/page/user/course_list.jsp");
	   close(null, pstmt, coon);
%>