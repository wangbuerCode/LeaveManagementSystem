<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
			 <% 
			 	//设置编码
			 	request.setCharacterEncoding("utf-8");
				//获取参数
				String instId = request.getParameter("instId");
				String instName = request.getParameter("instName");
				String depId = request.getParameter("depId");
				String telephone = request.getParameter("telephone");
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				int count = 0;
				
				//获得数据库连接 
				Connection coon = getConn();
				//创建语句 
				String sql = "insert into sys_instructor(instId,instName,depId,telephone,email,password) values(?,?,?,?,?,?)";
				PreparedStatement ptst = coon.prepareStatement(sql);
				ptst.setString(1, instId);
				ptst.setString(2, instName);
				ptst.setString(3, depId);
				ptst.setString(4, telephone);
				ptst.setString(5, email);
				ptst.setString(6, password);
				//执行语句 
				count = ptst.executeUpdate();
				//3:跳转到用户列表页面(user_list.jsp) 
				process(request, response,"/page/user/instructor_list.jsp");
				close(null, ptst, coon); 
				%>
