<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--删除用户--%>
<%
    //取出请求参数值之前设置请求的编码方式
      request.setCharacterEncoding("utf-8");
 	//接收参数
 	  int Count = 0;
      String userId = request.getParameter("userId");
      String password = request.getParameter("password");
      String fullname = request.getParameter("fullname");
      String telephone = request.getParameter("telephone");
     //获得数据库连接
      Connection coon = getConn();
      //创建语句
      String sql = "delete from sys_user where userId = ?";
      PreparedStatement ptmt = coon.prepareStatement(sql);
      ptmt.setString(1, userId);
      //执行更新
      Count = ptmt.executeUpdate();
  	//跳转到用户列表页面(user_list.jsp)
	process(request, response, "/page/user/user_list.jsp");
    close(null, ptmt, coon);
%>