<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>管理员信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder"> 管理员列表</strong>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th>工号</th>
					<th>姓名</th>
					<th>密码</th>
					<th>手机号</th>
					<th>操作</th>
				</tr>

				<%--读取所有管理员信息记录 --%>
				<%
					//设置编码方式
					request.setCharacterEncoding("utf-8");
					//2.1 获得链接
					Connection conn = getConn();
					String ss = (String)session.getAttribute("userid");
					String sql = "select * from sys_user where userId = ?";
					//2.3:由给定Connection类型的连接对象conn执行SQL指令的PrepareStatement类型的对象pstmt
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, ss);
					//2.4:PrepareStatement类型的对象pstmt执行SQL查询语句并返回结果
					ResultSet rs = pstmt.executeQuery();
					while (rs.next()) {
						String userId = rs.getString("userId");
						String fullname = rs.getString("fullname");
						String password = rs.getString("password");
						String telephone = rs.getString("telephone");
				%>

				<tr>
					<td><%=userId%></td>
					<td><%=fullname%></td>
					<td><%=password%></td>
					<td><%=telephone%></td>
					<td>
						<div class="button-group">
							<a class="button border-main"
								href="<%=path%>/page/user/user_edit.jsp?userId=<%=userId%>"><span
								class="icon-edit"></span> 修改</a> 
						</div>
					</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>

</body>
<script type="text/javascript">
$("[name='keywords']").each(function(){
var n = $(this).attr("search");
if(n.indexOf(name) == -1 )
{
$(this).hide();//隐藏不存在关键字的列表
}
});
</script>
</html>
