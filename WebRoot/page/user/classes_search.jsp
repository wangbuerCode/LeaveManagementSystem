<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>班级信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder">班级信息列表</strong>
			</div>
			<div class="padding border-bottom">
				<ul class="search" style="padding-left:10px;">
					<li>搜索：</li>
					<form  name="searchmessage" action="page/user/classes_search.jsp" method="post">
						<li><input type="text" placeholder="请输入搜索班级的班号"
						name="keywords" class="input" style="width:250px; line-height:17px;display:inline-block" /> 
						<a href='javascript:document.searchmessage.submit();' class="button border-main icon-search" > 搜索</a></li>
					</form>
				</ul>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th>班号</th>
					<th>班名</th>
					<th>二级学院编号</th>
					<th>专业</th>
					<th>年级</th>
				</tr>
				<%--读取所有管理员信息记录 --%>
				<%
					//设置编码方式
					request.setCharacterEncoding("utf-8");
					//
					String keywords = request.getParameter("keywords");
					//2.1 获得链接
					Connection conn = getConn();
					String sql = "select * from sys_classes where classId = ?";
					//2.3:由给定Connection类型的连接对象conn执行SQL指令的PrepareStatement类型的对象pstmt
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, keywords);
					//2.4:PrepareStatement类型的对象pstmt执行SQL查询语句并返回结果
					ResultSet rs = pstmt.executeQuery();
					//定义int序号变量
					while (rs.next()) {
						String classId = rs.getString("classId");
						String className = rs.getString("className");
						String depId = rs.getString("depId");
						String major = rs.getString("major");
						String grade = rs.getString("grade");
				%>

				<tr>
					<td><%=classId%></td>
					<td><%=className%></td>
					<td><%=depId%></td>
					<td><%=major%></td>
					<td><%=grade%></td>
					<td>
						<div class="button-group">
							<a class="button border-main"
								href="<%=path%>/page/user/classes_edit.jsp?classId=<%=classId%>"><span
								class="icon-edit"></span> 修改</a> 
						</div>
					</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>

</body>
<script type="text/javascript">
$("[name='keywords']").each(function(){
var n = $(this).attr("search");
if(n.indexOf(name) == -1 )
{
$(this).hide();//隐藏不存在关键字的列表
}
});
</script>
</html>
