<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>课程信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder">课程信息列表</strong>
			</div>
			<div class="padding border-bottom">
				<ul class="search" style="padding-left:10px;">
					<li>搜索：</li>
					<form  name="searchmessage" action="page/user/course_search.jsp" method="post">
						<li><input type="text" placeholder="请输入搜索课程名称"
						name="keywords" class="input" style="width:250px; line-height:17px;display:inline-block" /> 
						<a href='javascript:document.searchmessage.submit();' class="button border-main icon-search" > 搜索</a></li>
					</form>
				</ul>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th>课程编号</th>
					<th>班号</th>
					<th>课程名称</th>
					<th>学年</th>
					<th>学期</th>
					<th>学时</th>
					<th>操作</th>
				</tr>
				<%--读取所有管理员信息记录 --%>
				<%
					//设置编码方式
					request.setCharacterEncoding("utf-8");
					//2.1 获得链接
					Connection conn = getConn();
					String keywords = request.getParameter("keywords");
					String sql = "select * from sys_course where courseName = ?";
					//2.3:由给定Connection类型的连接对象conn执行SQL指令的PrepareStatement类型的对象pstmt
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, keywords);
					//2.4:PrepareStatement类型的对象pstmt执行SQL查询语句并返回结果
					ResultSet rs = pstmt.executeQuery();
					//定义int序号变量
					while (rs.next()) {
						String courseId = rs.getString("courseId");
						String classId = rs.getString("classId");
						String courseName = rs.getString("courseName");
						String year = rs.getString("year");
						String term = rs.getString("term");
						String hour = rs.getString("hour");
				%>

				<tr>
					<td><%=courseId%></td>
					<td><%=classId%></td>
					<td><%=courseName%></td>
					<td><%=year%></td>
					<td><%=term%></td>
					<td><%=hour%></td>
					<td>
						<div class="button-group">
						<a class="button border-main"
								href="<%=path%>/page/user/course_edit.jsp?courseId=<%=courseId%>"><span
								class="icon-edit"></span> 修改</a> 
						</div>
					</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>

</body>
<script type="text/javascript">
$("[name='keywords']").each(function(){
var n = $(this).attr("search");
if(n.indexOf(name) == -1 )
{
$(this).hide();//隐藏不存在关键字的列表
}
});
</script>
</html>
