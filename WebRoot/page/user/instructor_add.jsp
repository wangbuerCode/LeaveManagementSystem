<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>添加辅导员信息</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<%
   //根据参数读取数据库响应记录
   Connection conn=getConn();
   String sql="select * from sys_department";
   
   Statement stmt=conn.createStatement();
   ResultSet rs=stmt.executeQuery(sql);
   String rs_depId ="";
   String rs_depName ="";

   
   
 %>
<body>
	<div class="panel admin-panel">
		<div class="panel-head" id="add">
			<strong><span class="icon-pencil-square-o"></span>增加辅导员</strong>
		</div>
		<div class="body-content">
			<form method="post" class="form-x"
				action="page/user/action/action_addInstructor.jsp">
				<div class="form-group">
					<div class="label">
						<label>工号：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="instId"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>姓名：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="instName"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>二级学院名称：</label>
					</div>
					<div class="field">
						<select name="depId" class="input w50">
						<%  while(rs.next()){%>
							<option value="<%=rs.getString("depId") %>"><%=rs.getString("depName") %> </option>
							<%
							} %>
						</select>
					
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>联系电话：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="telephone"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>电子邮箱：</label>
					</div>
					<div class="field">
						<input type="email" class="input w50" value="" name="email"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>密码：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="password"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<button class="button bg-main icon-check-square-o" type="submit">
							提交</button>&emsp;&emsp;&nbsp;
						<a class="button bg-main icon-check-square-o" 
						href="page/user/instructor_list.jsp" 
						style="background-color:#FFB800; 
						type="submit">返回</a>
						</from>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>
