<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%@ page language="java" import="com.gxuwz.page.PageBean"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>请假信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder"> 请假列表</strong>
			</div>
			<div class="padding border-bottom">
				<ul class="search" style="padding-left:10px;">
					<li><a class="button border-main icon-plus-square-o"
						href="page/student/leave_add.jsp"> 填写请假单</a></li>
				</ul>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th>请假编号</th>
					<th>课程名称</th>
					<th>请假事由</th>
					<th>请假天数</th>
					<th>学号</th>
					<th>请假时间</th>
					<th>审核状态</th>
					<th>操作</th>
				</tr>

				<%--读取信息记录 --%>
				<%
					//设置编码方式
					request.setCharacterEncoding("utf-8");
					//2.1 获得链接
					Connection conn = getConn();
					String ss = (String)session.getAttribute("userid");
					String sql = "select * from sys_leave a ,sys_course b where a.courseId = b.courseId and stuNo = ?";
					//2.3:由给定Connection类型的连接对象conn执行SQL指令的PrepareStatement类型的对象pstmt
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, ss);
					//2.4:PrepareStatement类型的对象pstmt执行SQL查询语句并返回结果
					ResultSet rs = pstmt.executeQuery();
					//数据的行数row
					int row =0;
					//定义int序号变量
					int index = 1;
					while (rs.next()) {
						String leaveId = rs.getString("leaveId");
						String courseName = rs.getString("courseName");
						String reason = rs.getString("reason");
						String daynum = rs.getString("daynum");
						String stuNo = rs.getString("stuNo");
						int status = rs.getInt("status");
						String qpplytime = rs.getString("qpplytime");
						row++;
				%>
				<tr>
				<%
					String state = "";
						if (status == 0) {
							state = "待审核";
						} else if (status == 1) {
							state = "同意";
						} else if (status == 2) {
							state = "不同意";
						}
						if (state != null) {
				%>
					<td><%=leaveId%></td>
					<td><%=courseName%></td>
					<td><%=reason%></td>
					<td><%=daynum%></td>
					<td><%=stuNo%></td>
					<td><%=qpplytime%></td>
					<td><%=state%></td>
					<td>
					<% if(status!=1) {%>
						<div class="button-group">
								<a class="button border-main"
								href="<%=path%>/page/student/leave_edit.jsp?leaveId=<%=leaveId%>"><span
								class="icon-edit"></span> 修改</a> 
								<a class="button border-red"
								href="<%=path%>/page/student/action/action_delLeave.jsp?leaveId=<%=leaveId%>"><span
								class="icon-trash-o"></span> 删除</a>
						</div>
						<%} %>
					</td>
					<%} %>
				</tr>
				<%
					}
					//将数据条数存储到session中
					session.setAttribute("row", row);
				%>
			</table>
		</div>
<%--		<div class="pagelist">--%>
<%--		<%--%>
<%--			PageBean page1 = new PageBean();--%>
<%--			//获取总的数据条数--%>
<%--			String row1 = session.getAttribute("row").toString();--%>
<%--			int totoalrecord = Integer.parseInt(row1);--%>
<%--			//设置总的记录条数--%>
<%--			page1.setTotalRecord(totoalrecord);--%>
<%--			int totalRecord = page1.getTotalRecord();--%>
<%--			//每页显示的条数--%>
<%--			int pageSize = 5;--%>
<%--			//当前页数为--%>
<%--			int pageNum = 1;--%>
<%--			//调用service层，根据pageSize和pageNum查询数据库数据--%>
<%--			findAllwithpage(pageNum,pageSize);--%>
<%--			PageBean page2 = new PageBean(pageNum,pageSize,totalRecord);--%>
<%--			pageNum = page2.getPageNum();--%>
<%--			if(page1!=null){--%>
<%--		 %>--%>
<%--			共有<%=totalRecord%>条数据，共<%=page2.getTotalPage()%>页--%>
<%--			<a href="${pageContext.request.contextPath}/pageNum=1">首页</a>--%>
<%--			<a href="#">上一页</a>--%>
<%--			<span class="current">1</span>--%>
<%--		    <a href="#">2</a>--%>
<%--		    <a href="#">3</a>--%>
<%--		    <a href="#">下一页</a>--%>
<%--		    <a href="#">尾页</a>--%>
<%--		    --%>
<%--		 <%} %>--%>
<%--		</div>--%>
</body>
<script type="text/javascript">
$("[name='keywords']").each(function(){
var n = $(this).attr("search");
if(n.indexOf(name) == -1 )
{
$(this).hide();//隐藏不存在关键字的列表
}
});
</script>
</html>
