<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<% 
   //接收参数
   String leaveId = request.getParameter("leaveId");
   //根据参数读取数据库响应记录
   Connection conn=getConn();
   String sql="select * from sys_leave where leaveId = ?";
   PreparedStatement pstmt=conn.prepareStatement(sql);
   pstmt.setString(1, leaveId);
   ResultSet rs=pstmt.executeQuery();
   String rs_leaveId ="";
   String rs_courseId ="";
   String rs_reason  ="";
   String rs_daynum  ="";
   String rs_stuNo  ="";
   String rs_qpplytime ="";
   if(rs.next()){
   		   rs_leaveId = rs.getString("leaveId");
	       rs_courseId = rs.getString("courseId");
	       rs_reason = rs.getString("reason");
	       rs_daynum = rs.getString("daynum");
	       rs_stuNo = rs.getString("stuNo");
	       rs_qpplytime = rs.getString("qpplytime");
   }
   //使用表达式显示
   //跳转，无
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>请假单列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head" id="add">
			<strong><span class="icon-pencil-square-o"></span>编辑请假单</strong>
		</div>
		<div class="body-content">
			<form method="post" class="form-x"
				action="page/student/action/action_editLeaver.jsp">
				<div class="form-group">
					<div class="label">
						<label>请假编号：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" readonly="readonly"
							value="<%=rs_leaveId %>" name="leaveId" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>课程编号：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" readonly="readonly"
							value="<%=rs_courseId %>" name="courseId" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>课程名称：</label>
					</div>
					<div class="field">
						<select name="courseName" class="input w50">
							<%
								Connection conn1 = getConn();
								String sql1 = "select * from sys_course where courseId = ?";
								PreparedStatement pstmt1 = conn1.prepareStatement(sql1);
								pstmt1.setString(1, rs_courseId);
								ResultSet rs1 = pstmt1.executeQuery();
								while (rs1.next()) {
									String courseName = rs1.getString("courseName");
							%>
							<option value="<%=courseName%>"><%=courseName%>
								<%
									}
								%>
								
								<%
								Connection conn2 = getConn();
								String sql2 = "select * from sys_course where courseId != ?";
								PreparedStatement pstmt2 = conn2.prepareStatement(sql2);
								pstmt2.setString(1, rs_courseId);
								ResultSet rs2 = pstmt2.executeQuery();
								while (rs2.next()) {
									String courseName = rs2.getString("courseName");
							%>
							<option value="<%=courseName%>"><%=courseName%>
								<%
									}
								%>
							</option>
						</select>

						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>请假事由：</label>
					</div>
					<div class="field" style="width:600px;">
						<textarea class="input" name="reason"  height:90px;"><%=rs_reason %></textarea>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>请假天数：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_daynum %>" name="daynum"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>学号：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="<%=rs_stuNo %>" name="stuNo"readonly="readonly"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>请假时间：</label>
					</div>
					<div class="field" >
						<input type="date-local" class="input w50"  value="<%=rs_qpplytime %>" 
						name="qpplytime" data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<button class="button bg-main icon-check-square-o" type="submit">
							提交</button>
						<a class="button bg-main icon-check-square-o"
							href="page/student/leave_list.jsp"
							style="background-color:#FFB800; 
						type="submit">返回</a>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>
