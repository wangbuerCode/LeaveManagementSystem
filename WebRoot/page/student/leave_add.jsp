<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>添加管理员信息</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head" id="add">
			<strong><span class="icon-pencil-square-o"></span>请假单</strong>
		</div>
		<div class="body-content">
			<form method="post" class="form-x"
				action="page/student/action/action_addLeaver.jsp">
				<div class="form-group">
					<div class="label">
						<label>课程名称：</label>
					</div>
					<div class="field">
						<select name="courseName" class="input w50">
							<%
								Connection coon = getConn();
								String sql = "select * from sys_course";
								Statement stmt = coon.createStatement();
								ResultSet rs = stmt.executeQuery(sql);
								while (rs.next()) {
									String courseName = rs.getString("courseName");
							%>
							<option value="<%=courseName%>"><%=courseName%>
								<%
									}
								%>
							</option>
						</select>

						<div class="tips"></div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="label">
						<label>请假事由：</label>
					</div>
					<div class="field" style="width:600px;">
						<textarea class="input" name="reason"  style="height:90px;"></textarea>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>请假天数：</label>
					</div>
					<div class="field">
						<input type="text" class="input w50" value="" name="daynum"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group" >
					<div class="label">
						<label>请假时间：</label>
					</div>
					<div class="field" >
						<input  id="clock" type="datetime-local" class="input w50"  value="" name="qpplytime"
							data-validate="required:数据必填项" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
					<button class="button bg-main icon-check-square-o" type="submit">
							提交</button>
						&emsp;&emsp;&nbsp; <a class="button bg-main icon-check-square-o"
							href="page/student/leave_list.jsp"
							style="background-color:#FFB800; 
						type="submit">返回</a>
						</from>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
<script type="text/javascript">
	function showTime() { 
	var date1 =new Date;
    var year=date1.getFullYear();
    var month=date1.getMonth();
    var day=date1.getDate();
    var hh=date1.getHours();
    var mm=date1.getMinutes();
    var ss=date1.getSeconds();
	var time=year+"年"+(month+1)+"月"+day+"日  "+hh+":"+mm+":"+ss;
    document.getElementById("clock").innerHTML = time;
	}
</script>
</html>
