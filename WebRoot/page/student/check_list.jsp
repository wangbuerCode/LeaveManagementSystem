 <%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>请假信息列表</title>
<link rel="stylesheet" href="css/pintuer.css">
<link rel="stylesheet" href="css/admin.css">
<link rel="shortcut icon" href="images/logo.jpg">
<script src="js/jquery.js"></script>
<script src="js/pintuer.js"></script>

</head>

<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> 请假列表</strong>
		</div>
		<table class="table table-hover text-center">
			<tr>
				<th>请假编号</th>
				<th>姓名</th>
				<th>课程名称</th>
				<th>请假事由</th>
				<th>请假天数</th>
				<th>学号</th>
				<th>请假时间</th>
				<th>审核状态</th>
				<th>审核时间</th>
				<th>审核意见</th>
			</tr>

			<%--读取所有管理员信息记录 --%>
			<%
				//设置编码方式
				request.setCharacterEncoding("utf-8");
				//2.1 获得链接
				Connection conn = getConn();
				String sql = "select * from sys_leave a ,sys_course b,sys_student c"+
				" where a.courseId = b.courseId and a.stuNo = c.stuId and stuId = ?";
				//2.3:由给定Connection类型的连接对象conn执行SQL指令的PrepareStatement类型的对象pstmt
				PreparedStatement pstmt = conn.prepareStatement(sql);
				String ss = (String)session.getAttribute("userid");
				pstmt.setString(1, ss);
				//2.4:PrepareStatement类型的对象pstmt执行SQL查询语句并返回结果
				ResultSet rs = pstmt.executeQuery();
				//定义int序号变量
				while (rs.next()) {
					String leaveId = rs.getString("leaveId");
					String stuName = rs.getString("stuName");
					String courseName = rs.getString("courseName");
					String reason = rs.getString("reason");
					int daynum = rs.getInt("daynum");
					String stuNo = rs.getString("stuNo");
					String qpplytime = rs.getString("qpplytime");
					int status = rs.getInt("status");
					String audittime = "";
					if (rs.getString("audittime")==null){
						audittime = "审核中...";
					} else{
						audittime = rs.getString("audittime");
					}
					String opinion ="";
					if (status!=0 && rs.getString("opinion")==null){
						opinion = "无意见";
					} else if(status == 0 && rs.getString("opinion")==null){
						opinion = "暂无";
					}else{
						opinion = rs.getString("opinion");
					}
			%>
			<tr>
				<%
					String state = "";
						if (status == 0) {
							state = "待审核";
						} else if (status == 1) {
							state = "同意";
						} else if (status == 2) {
							state = "不同意";
						}
						if (state != null) {
				%>
				<td><%=leaveId%></td>
				<td><%=stuName%></td>
				<td><%=courseName%></td>
				<td><%=reason%></td>
				<td><%=daynum%></td>
				<td><%=stuNo%></td>
				<td><%=qpplytime%></td>
				<td><%=state%></td>
				<td><%=audittime%></td>
				<td><%=opinion%></td>
				<%
					}
				%>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	

</body>
<script type="text/javascript">
	$("[name='keywords']").each(function() {
		var n = $(this).attr("search");
		if (n.indexOf(name) == -1) {
			$(this).hide(); //隐藏不存在关键字的列表
		}
	});
</script>
</html>
