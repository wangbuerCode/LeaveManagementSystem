<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp" %>
<%@ include file="/base/database.jsp" %>
<%--添加用户过程--%>
<%
    //取出请求参数值之前设置请求的编码方式
       request.setCharacterEncoding("utf-8");
 	//接收参数
 	  String leaveId = request.getParameter("leaveId");
      String courseId = request.getParameter("courseId");
      String courseName = request.getParameter("courseName");
	  String reason = request.getParameter("reason");
	  String daynum = request.getParameter("daynum");
	  String stuNo = request.getParameter("stuNo");
	  String qpplytime = request.getParameter("qpplytime");
      int count = 0;
    //获得数据库连接
    Connection coon = getConn();
    //根据courseName查询提取courseId
    String sql1 = "select * from sys_course where courseName = ?";
    PreparedStatement pstmt1 = coon.prepareStatement(sql1);
    pstmt1.setString(1, courseName);
    ResultSet rs1 = pstmt1.executeQuery();
    while(rs1.next()) {
    	 courseId = rs1.getString("courseId");
    }
    //进行更新
   	String sql = "update sys_leave set courseId=?,reason=?,daynum=?,stuNo=?,qpplytime=?"
			 	+ "where leaveId = ?";
	PreparedStatement pstmt = coon.prepareStatement(sql);
    pstmt.setString(1, courseId);
    pstmt.setString(2, reason);
    pstmt.setString(3, daynum);
    pstmt.setString(4, stuNo);
    pstmt.setString(5, qpplytime);
    pstmt.setString(6, leaveId);
    count = pstmt.executeUpdate();
  	//跳转到用户列表页面(leave_list.jsp)
	  process(request, response, "/page/student/leave_list.jsp");
%>