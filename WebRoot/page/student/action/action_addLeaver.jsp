<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--添加用户过程--%>
			 <% 
			 	//设置编码
			 	request.setCharacterEncoding("utf-8");
				//获取参数
				// 获取请假编号的随机数，调用一个方法
				String leaveId = leaveNum();
				String courseName = request.getParameter("courseName");
				String reason = request.getParameter("reason");
				String daynum = request.getParameter("daynum");
				String stuNo = request.getParameter("stuNo");
				String qpplytime = request.getParameter("qpplytime");
				String ss = session.getAttribute("userid").toString();
				int count = 0;
				int count1 = 0; 
				
				//获得数据库连接 
				Connection coon = getConn();
				//创建语句 
				String sql = "select * from sys_course where courseName = ?";
				PreparedStatement ptst1= coon.prepareStatement(sql);
				ptst1.setString(1, courseName);
				ResultSet rs = ptst1.executeQuery();
				if(rs.next()) {
					String courseId1 = rs.getString("courseId");
					String sql1 = "insert into sys_leave(leaveId,courseId,reason,daynum,stuNo,qpplytime)"+
				"values(?,?,?,?,?,?)";
				PreparedStatement ptst = coon.prepareStatement(sql1);
				ptst.setString(1, leaveId);
				ptst.setString(2, courseId1);
				ptst.setString(3, reason);
				ptst.setString(4, daynum);
				ptst.setString(5, ss);
				ptst.setString(6, qpplytime);
				//执行语句 
				count = ptst.executeUpdate();
				}
				
				
				//对深刻意见进行更新
				Connection conn1 = getConn();
				String sql2 = "select * from sys_leave";
				Statement stmt = conn1.createStatement();
				ResultSet rs2 = stmt.executeQuery(sql2);
				while (rs2.next()) {
					String opinion =rs2.getString("opinion");
					//一开始申请意见为null，所以要将null改为暂无
					if(opinion==null) {
						 opinion = "暂无";
					} else {
						opinion = rs2.getString("opinion");
					}	
					Connection conn2 = getConn();
					String sql3 = "update sys_leave set opinion = ?";
					PreparedStatement pstmt = conn2.prepareStatement(sql3);
					pstmt.setString(1,opinion);
					count1 = pstmt.executeUpdate();
				}
				//3:跳转到用户列表页面(leave_list.jsp) 
				process(request, response,"/page/student/leave_list.jsp"); 
				%>
