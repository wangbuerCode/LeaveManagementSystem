<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/base.jsp"%>
<%@ include file="/base/database.jsp"%>
<%--删除请假单--%>
<%
	//取出请求参数值之前设置请求的编码方式
	request.setCharacterEncoding("utf-8");
	//接收参数
	int Count = 0;
	String leaveId = request.getParameter("leaveId");
	String courseId = request.getParameter("courseId");
	String reason = request.getParameter("reason");
	String daynum = request.getParameter("daynum");
	String stuNo = request.getParameter("stuNo");
	String qpplytime = request.getParameter("qpplytime");
	//获得数据库连接
	Connection coon = getConn();
	//创建语句
	String sql = "delete from sys_leave where leaveId = ?";
	PreparedStatement ptmt = coon.prepareStatement(sql);
	ptmt.setString(1, leaveId);
	//执行更新
	Count = ptmt.executeUpdate();
	//跳转到用户列表页面(leave_list.jsp)
	process(request, response, "/page/student/leave_list.jsp");
%>