<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/base/database.jsp"%>
<%@ include file="/base/base.jsp"%>

<%	
	String url = "";
	String str = "";
	String userid = request.getParameter("userid");
	String password = request.getParameter("password");
	String type = request.getParameter("users");
	//将登陆id存在session里
	session.setAttribute("userid", userid);
	session.setAttribute("password", password);
	Connection conn = getConn();
	String str1 = " select * from sys_student where stuId=? and password=?  ";
	String str2 = " select * from sys_instructor where instId=? and password=?  ";
	String str3 = " select * from sys_user where userId=? and password=? ";

	if(type.equals("1")) {
		str = str1;
		url = "/page/student/stu_login.jsp";
	}else if (type.equals("2")){
		str = str2;
		url = "/page/instructor/instructor_login.jsp";
	}else {
		str = str3;
		url = "/page/user/user_login.jsp";
	}
	PreparedStatement pst = conn.prepareStatement(str);
	pst.setString(1,userid);
	pst.setString(2,password);
	ResultSet rs = pst.executeQuery();
	if (rs.next()) {
			process(request, response, url);
	}else {
		out.write("用户名或密码输入错误！");
		response.setHeader("refresh","2;url=/leave/login.jsp");
	}
%>