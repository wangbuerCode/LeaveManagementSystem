
/** 
 * projectName: leave 
 * fileName: Excel.java 
 * packageName: leave 
 * date: 2019年5月5日上午8:28:06 
 * copyright(c) 2017-2020 xxx公司
 */
package com.gxuwz.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**   
 * @title: Excel.java 
 * @package leave 
 * @description: TODO
 * @author: 梧州学院 软件开发中心 梁树鹏
 * @date: 2019年5月5日 上午8:28:06 
 * @version: V1.0   
*/
public class Excel {
	public void export(List<HashMap<String,Object>> list,String path) {
	  	String []title = {"请假编号","班级名称","学期","课程名称","请假事由","请假天数","学号","请假时间","审核状态","审核意见"};
	  	//创建一个工作簿对象
	  	WritableWorkbook wb = null;
	  	
	  	String fileName = path;
	  	File file = new File(fileName);
				try {
					wb = jxl.Workbook.createWorkbook(file);
					//创建工作表对象
			  		WritableSheet sheet = wb.createSheet("学生请假单统计表", 0);
			  		//生成表头
			  		Label label = null;
			  		for(int i = 0;i<title.length;i++) {
			  			//第0行第i列
			  			label = new Label(i,0,title[i]);
							sheet.addCell(label);
			  		}
			  	//追加数据
			  		Label label2 = null;
			  		HashMap<String,Object> map = null;
			  		for(int i = 0;i<list.size();i++) {
			  			map = list.get(i);
			  			//往第i+1行的第一列格子里追击数据
			  			label2 = new Label(0,i+1,String.valueOf(map.get("leaveId")));
			  			label2 = new Label(1,i+1,String.valueOf(map.get("className")));
			  			label2 = new Label(2,i+1,String.valueOf(map.get("term")));
			  			label2 = new Label(3,i+1,String.valueOf(map.get("courseName")));
			  			label2 = new Label(4,i+1,String.valueOf(map.get("reason")));
			  			label2 = new Label(5,i+1,String.valueOf(map.get("daynum")));
			  			label2 = new Label(6,i+1,String.valueOf(map.get("stuNo")));
			  			label2 = new Label(7,i+1,String.valueOf(map.get("qpplytime")));
			  			label2 = new Label(8,i+1,String.valueOf(map.get("status")));
			  			label2 = new Label(9,i+1,String.valueOf(map.get("audittime")));
			  			label2 = new Label(10,i+1,String.valueOf(map.get("opinion")));
			  		}
			  		//写入数据
			  		wb.write();
			  		//关闭工作簿
			  		wb.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (WriteException e) {
					
					e.printStackTrace();
				}
  	  } 
}
